#include<iostream>
#include<string>
#include<conio.h>
#include<time.h>
#include "UnorderedArray.h"

using namespace std;

UnorderedArray<int> generateUnorderedArray(UnorderedArray<int> ray) {

	return ray;
}

void displayArray(UnorderedArray<int> ray) {
	cout << "Displaying Array\nSize: " << ray.getSize() << endl << endl;
	for (int i = 0; i < ray.getSize(); i++)
		cout << ray[i] << endl;
}

int main() {
	srand(time(NULL));
	int size;
	cout << "What is the size of the Array?\nSize: ";
	cin >> size;
	UnorderedArray<int> unordered(size);

	cout << endl;

	for (int i = 0; i < unordered.getSize(); i++) {
		unordered.push(rand() % 100 + 1);
	}

	cout << "Displaying Array\nSize: " << unordered.getSize() << endl;
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << endl;
	cout << endl;

	cout << "which element would you like to delete?\n";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << "[" << i << "]\t" << unordered[i] << endl;

	int input;
	do cin >> input; while (input >= unordered.getSize() || input < 0);
	unordered.remove(input);
	cout << "Displaying Array\nSize: " << unordered.getSize() << endl;
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << endl;
	cout << endl;

	cout << "what value would you like to search\nsearch: ";
	cin >> input;
	int value = unordered.linearSearch(input);
	if (value < 0) cout << "value is not found\n";
	else cout << "value is found at element " << value << endl;

	_getch();


}